package com.ashish.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static WebDriver obj;
        public static void main(String[] args) {

            obj = setUpDriver();
            obj.get("https://www.myntra.com/");
            obj.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
            obj.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));

            obj.manage().window().maximize();
            try {
                orderTshirt();
            } catch (Exception e) {
                e.printStackTrace();
            }
            obj.quit();
        }

        public static WebDriver setUpDriver() {
            System.setProperty("webdriver.chrome.driver", "/home/ashish/Selenium/chromedriver");
            WebDriver driver = new ChromeDriver();
            return driver;
        }

        public static void orderTshirt() {
            try {
                obj.findElement(By.cssSelector("input[placeholder='Search for products, brands and more']"))
                        .sendKeys("Tshirt");
                Thread.sleep(3000);

                // click on the search button
                obj.findElement(By.className("desktop-submit")).click();
                Thread.sleep(3000);

                // select the tshirt.
                obj.findElement(By.className("img-responsive")).click();

                Thread.sleep(3000);
                List<String> newTb = new ArrayList<>(obj.getWindowHandles());

                // click on select the size.
                obj.switchTo().window(newTb.get(1)).
                        findElement(By.xpath("(//button[contains(@class,'size-button-default')]//p)[1]")).click();
                Thread.sleep(3000);

                //  add to bag.
                obj.findElement(By.xpath("//*[contains(text(), 'ADD TO BAG')]")).click();
                Thread.sleep(3000);

                // click bag button.
                obj.findElement(By.xpath("//span[text()='Bag']")).click();
                Thread.sleep(3000);

                //click anywhere when pop-up come
                obj.findElement(By.xpath("//html")).click();
                Thread.sleep(5000);

                // click place order.
                obj.findElement(By.xpath("//div[text()='Place Order']")).click();
                Thread.sleep(3000);

                // fill the phone number.
                obj.findElement(By.xpath("//input[@type='tel']")).sendKeys("8878719685");
                Thread.sleep(3000);

                // click on continue
                obj.findElement(By.xpath("//*[@id=\"reactPageContent\"]/div/div/div[2]/div[2]/div[3]")).click();
                Thread.sleep(10000);

                //click on password
                obj.findElement(By.xpath("//*[@id=\"reactPageContent\"]/div/div[3]/span")).click();

                // to fill password.
                try {
                    WebElement password = obj.
                            findElement(By.xpath("//*[@id='reactPageContent']/div/div/form/div/div[1]/input"));
                    if (password.isDisplayed()) {
                        password.sendKeys("Ashish@125");
                        Thread.sleep(4000);

                        // It will click the submit button.
                        obj.findElement(By.xpath("//*[@id='reactPageContent']/div/div/form/div/div[2]/button")).click();
                        Thread.sleep(20000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // It will click on the place order button.
                obj.findElement(By.xpath("//*[ contains( text(),'Place Order')]")).click();
                Thread.sleep(5000);

                obj.findElement(By.id("placeOrderButton")).click();
                Thread.sleep(10000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
